/*
 * DWT.c
 *
 *  Created on: 19Dec.,2016
 *      Author: akoiro
 */

#include "stm32f1xx_hal.h"
#include "DWT.h"

uint32_t __DWT_Get(void) {
  return DWT->CYCCNT;
}

uint8_t __DWT_Compare(int32_t tp) {
  return (((int32_t)__DWT_Get() - tp) < 0);
}


void DWT_Init(void) {
  //if (!(CoreDebug->DEMCR & CoreDebug_DEMCR_TRCENA_Msk))
  {
    CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CYCCNT = 0;
    DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;
  }
}

// microseconds
void DWT_Delay(uint32_t us) {
	uint32_t tp = __DWT_Get() + us * (SystemCoreClock/1000000);
	while (__DWT_Compare(tp));
}


