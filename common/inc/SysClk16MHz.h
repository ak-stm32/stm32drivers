/*
 * SysClk16MHz.h
 *
 *  Created on: 30Dec.,2016
 *      Author: akoiro
 */

#include "stm32f1xx_hal.h"

#ifndef SYSCLK16MHZ_H_
#define SYSCLK16MHZ_H_

/**
 * SYSCLK - 16MHz
 * HCLK - 16MHz
 * FCLK - 16MHz
 * APB1 Perph - 16Mhz
 * APB1 Timer - 16Mhz
 * APB2 Perph - 16Mhz
 * APB2 Timer - 16Mhz
 */
void SystemClock_HSE_PLLx2_16MHz(void);

/**
 * SYSCLK - 8MHz
 * HCLK - 8MHz
 * FCLK - 8MHz
 * APB1 Perph - 8Mhz
 * APB1 Timer - 8Mhz
 * APB2 Perph - 8Mhz
 * APB2 Timer - 8Mhz
 */
void SystemClock_8MHz(void);

#endif /* SYSCLK16MHZ_H_ */
