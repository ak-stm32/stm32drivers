/*
 * AK_COMMON.h
 *
 *  Created on: 9 ���. 2016 �.
 *      Author: pervoliner
 */

#ifndef AK_COMMON_H_
#define AK_COMMON_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "stm32f1xx.h"
#include "stm32f1xx_it.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_def.h"

void __AK_GPIO_PORT_ENABLE(GPIO_TypeDef*);

void __AK_TIM_CLK_ENABLE(TIM_TypeDef*);
void __AK_TIM_CLK_DISABLE(TIM_TypeDef*);

void __AK_SPI_CLK_ENABLE(SPI_TypeDef * spi);
void __AK_SPI_CLK_DISABLE(SPI_TypeDef * spi);

typedef struct {
	GPIO_TypeDef *port;
	uint32_t pin;
} GPIOPin_Def;


#endif /* AK_COMMON_H_ */
