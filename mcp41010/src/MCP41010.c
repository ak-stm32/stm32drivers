/*
 * MCP41010.c
 *
 *  Created on: 16 Oct 2016
 *      Author: akoiro
 */

#include "MCP41010.h"

//#define MCP41010_DMA

#ifdef MCP41010_DMA
DMA_HandleTypeDef MCP41010_hdma_spi1_tx;
#endif

void _MCP41010_GPIO_Init(MCP41010_HandleDef* handle);
#ifdef MCP41010_DMA
void _MCP41010_DMA_Init(MCP41010_HandleDef* handle);
void _MCP41010_DMA_MspInit(MCP41010_HandleDef* handle);
#endif

void MCP41010_Write(uint8_t value, MCP41010_HandleDef* handle) {
	handle->Value[1] = value;
	HAL_GPIO_WritePin(handle->GPIOType, handle->NSS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(&handle->SPIHandle, handle->Value, 2, 1000);
	HAL_GPIO_WritePin(handle->GPIOType, handle->NSS_Pin, GPIO_PIN_SET);
}

void MCP41010_Init(MCP41010_HandleDef* handle) {
	handle->Value[0] = 0B00010001;
	handle->Value[1] = 0B00000000;

	_MCP41010_GPIO_Init(handle);

#ifdef _MCP41010_DMA
	_MCP41010_DMA_Init(handle);
#endif

	handle->SPIHandle.Instance = handle->SPIType;
	handle->SPIHandle.Init.Mode = SPI_MODE_MASTER;
	handle->SPIHandle.Init.Direction = SPI_DIRECTION_2LINES;
	handle->SPIHandle.Init.DataSize = SPI_DATASIZE_8BIT;
	handle->SPIHandle.Init.CLKPolarity = SPI_POLARITY_LOW;
	handle->SPIHandle.Init.CLKPhase = SPI_PHASE_1EDGE;
	handle->SPIHandle.Init.NSS = SPI_NSS_SOFT;
	handle->SPIHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
	handle->SPIHandle.Init.FirstBit = SPI_FIRSTBIT_MSB;
	handle->SPIHandle.Init.TIMode = SPI_TIMODE_DISABLE;
	handle->SPIHandle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	handle->SPIHandle.Init.CRCPolynomial = 10;
	if (HAL_SPI_Init(&handle->SPIHandle) != HAL_OK) {
		Error_Handler();
	}
}

void MCP41010_MspInit(MCP41010_HandleDef* handle) {
	__AK_SPI_CLK_ENABLE(handle->SPIType);

	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin = handle->MOSI_Pin | handle->SCK_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(handle->GPIOType, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = handle->NSS_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(handle->GPIOType, &GPIO_InitStruct);
	HAL_GPIO_WritePin(handle->GPIOType, handle->NSS_Pin, GPIO_PIN_SET);

#ifdef MCP41010_DMA
	_MCP41010_HAL_DMA_MspInit(spiHandle);
#endif
}

void MCP41010_MspDeInit(MCP41010_HandleDef* handle) {
	__AK_SPI_CLK_DISABLE(handle->SPIType);

	HAL_SPI_DeInit(&handle->SPIHandle);

	HAL_GPIO_DeInit(handle->GPIOType,
			handle->MOSI_Pin | handle->SCK_Pin | handle->NSS_Pin);

#ifdef MCP41010_DMA
	HAL_DMA_DeInit(spiHandle->hdmatx);
#endif
}

void _MCP41010_GPIO_Init(MCP41010_HandleDef* handle) {
	__AK_GPIO_PORT_ENABLE(handle->GPIOType);
}

#ifdef MCP41010_DMA
void _MCP41010_DMA_Init(MCP41010_HandleDef* handle) {
	HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);
}

void _MCP41010_DMA_MspInit(MCP41010_HandleDef* handle) {
	hdma_spi1_tx.Instance = DMA1_Channel3;
	hdma_spi1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
	hdma_spi1_tx.Init.PeriphInc = DMA_PINC_DISABLE;
	hdma_spi1_tx.Init.MemInc = DMA_MINC_ENABLE;
	hdma_spi1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	hdma_spi1_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	hdma_spi1_tx.Init.Mode = DMA_NORMAL;
	hdma_spi1_tx.Init.Priority = DMA_PRIORITY_LOW;
	if (HAL_DMA_Init(&hdma_spi1_tx) != HAL_OK) {
		Error_Handler();
	}
	__HAL_LINKDMA(spiHandle, hdmatx, hdma_spi1_tx);
}
#endif

