/*
 * MAX7219.h
 *
 *  Created on: 15Apr.,2017
 *      Author: akoiro
 */
#include "COMMON.h"
#include "stm32f1xx.h"
#include "stm32f1xx_it.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_def.h"

#ifndef MAX7219_INIT_H_
#define MAX7219_INIT_H_

typedef struct {
	//should be defined before init, COUNT%2 always 0
	uint8_t COUNT;

	//should be defined before init, size should be COUNT*2
	uint8_t* SPI_VALUE;

	SPI_TypeDef *SPI;
	IRQn_Type DMA_IRQ;

	uint16_t PIN_MOSI;
	uint16_t PIN_SCK;
	uint16_t PIN_NSS;
	GPIO_InitTypeDef PINS;
	GPIO_TypeDef* PORT;

	SPI_HandleTypeDef SPI_HANDLE;
	DMA_HandleTypeDef DMA_HANDLE;

} MAX7219_Def;

void MAX7219_SPI1_Init(MAX7219_Def*);
void MAX7219_SPI2_Init(MAX7219_Def*);
void MAX7219_Init(MAX7219_Def*);
void MAX7219_MspInit(MAX7219_Def*);
void MAX7219_MspDeInit(MAX7219_Def*);
void MAX7219_TxCallback(MAX7219_Def*);

#endif /* MAX7219_INIT_H_ */
