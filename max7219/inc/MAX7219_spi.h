/*
 * MAX7219_spi.h
 *
 *  Created on: 15Apr.,2017
 *      Author: akoiro
 */
#include "COMMON.h"
#include "MAX7219_init.h"

#ifndef MAX7219_SPI_H_
#define MAX7219_SPI_H_

void MAX7219_Transmit_SPI(MAX7219_Def* def, uint8_t* data, uint16_t size);
void MAX7219_NSS_ON(MAX7219_Def*);
void MAX7219_NSS_OFF(MAX7219_Def*);


#endif /* MAX7219_SPI_H_ */
