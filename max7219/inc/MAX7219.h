/*
 * MAX7219.h
 *
 *  Created on: 15Apr.,2017
 *      Author: akoiro
 */
#include "COMMON.h"
#include "MAX7219_init.h"
#include "MAX7219_spi.h"



#ifndef MAX7219_H_
#define MAX7219_H_

typedef enum {
	Address_NOOP = 0x00,
	Address_Digit0 = 0x01,
	Address_Digit1 = 0x02,
	Address_Digit2 = 0x03,
	Address_Digit3 = 0x04,
	Address_Digit4 = 0x05,
	Address_Digit5 = 0x06,
	Address_Digit6 = 0x07,
	Address_Digit7 = 0x08,
	Address_DecodeMode = 0x09,
	Address_Intensity = 0x0A,
	Address_ScanLimit = 0x0B,
	Address_Shutdown = 0x0C,
	Address_DisplayTest = 0x0F

} MAX7219_Address;

typedef enum {
	DecodeMode_NoBCD = 0x00,
	DecodeMode_BCD0 = 0x01,
	DecodeMode_BCD3_0 = 0x0F,
	DecodeMode_BCDAll = 0xFF
} MAX7219_DecodeMode;

typedef enum {
	Intensity_1_32 = 0x00,
	Intensity_3_32 = 0x01,
	Intensity_5_32 = 0x02,
	Intensity_7_32 = 0x03,
	Intensity_9_32 = 0x04,
	Intensity_11_32 = 0x05,
	Intensity_13_32 = 0x06,
	Intensity_15_32 = 0x07,
	Intensity_17_32 = 0x08,
	Intensity_19_32 = 0x09,
	Intensity_21_32 = 0x0A,
	Intensity_23_32 = 0x0B,
	Intensity_25_32 = 0x0C,
	Intensity_27_32 = 0x0D,
	Intensity_29_32 = 0x0E,
	Intensity_31_32 = 0x0F,
} MAX7219_Intensity;

typedef enum {
	Scan0 = 0x00,
	Scan0_1 = 0x01,
	Scan0_2 = 0x02,
	Scan0_3 = 0x03,
	Scan0_4 = 0x04,
	Scan0_5 = 0x05,
	Scan0_6 = 0x06,
	Scan0_7 = 0x07,
} MAX7219_ScanLimit;

typedef enum {
	CodeB_DOT = 0xF0,
	CodeB_Minus = 0x0A,
	CodeB_E = 0x0B,
	CodeB_H = 0x0C,
	CodeB_L = 0x0D,
	CodeB_P = 0x0E,
	CodeB_EMPTY = 0x0F
} MAX7219_CodeB;


typedef struct {
	uint8_t chip;
	uint32_t value;
	bool leadZero;
	uint8_t dotDigit;
	bool useTwo;
} MAX7219_Value;

void MAX7219_SetValueToAll(MAX7219_Def* def, MAX7219_Address address, uint8_t value);
void MAX7219_SetIntValue(MAX7219_Def*, MAX7219_Value*);
void MAX7219_Reset(MAX7219_Def*);

#endif /* MAX7219_H_ */
