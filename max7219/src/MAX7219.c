/*
 * MAX7219.c
 *
 *  Created on: 10Apr.,2017
 *      Author: akoiro
 */
#include "MAX7219.h"

#include "DWT.h"

void __MAX7219_Transmit(MAX7219_Def* def, uint8_t chip, uint8_t address,
		uint8_t value);
void _MAX7219_SetIntValue(MAX7219_Def* def, MAX7219_Value* value);

uint8_t V[4] = { 0b00000000, 0b00000000, 0b00000000, 0b00000000 };

void MAX7219_SetIntValue(MAX7219_Def* def, MAX7219_Value* value) {
	_MAX7219_SetIntValue(def, value);
}

void MAX7219_SetValueToAll(MAX7219_Def* def, MAX7219_Address address,
		uint8_t data) {
	for (uint8_t i = 0; i < def->COUNT; i++) {
		def->SPI_VALUE[i * 2] = address;
		def->SPI_VALUE[i * 2 + 1] = data;
	}
	MAX7219_Transmit_SPI(def, def->SPI_VALUE, def->COUNT * 2);
}

void MAX7219_Reset(MAX7219_Def* def) {
	MAX7219_SetValueToAll(def, Address_DisplayTest, 0x01);
	MAX7219_SetValueToAll(def, Address_DisplayTest, 0x00);
	MAX7219_SetValueToAll(def, Address_ScanLimit, Scan0_7);
	MAX7219_SetValueToAll(def, Address_DecodeMode, DecodeMode_NoBCD);
	MAX7219_SetValueToAll(def, Address_Intensity, Intensity_31_32);
	MAX7219_SetValueToAll(def, Address_Shutdown, 0x01);
	MAX7219_SetValueToAll(def, Address_Digit0, 0x0F);
	MAX7219_SetValueToAll(def, Address_Digit1, 0x0F);
	MAX7219_SetValueToAll(def, Address_Digit2, 0x0F);
	MAX7219_SetValueToAll(def, Address_Digit3, 0x0F);
	MAX7219_SetValueToAll(def, Address_Digit4, 0x0F);
	MAX7219_SetValueToAll(def, Address_Digit5, 0x0F);
	MAX7219_SetValueToAll(def, Address_Digit6, 0x0F);
	MAX7219_SetValueToAll(def, Address_Digit7, 0x0F);
}

//logic
void __MAX7219_TransmitToChip(MAX7219_Def* def, uint8_t chip,
		MAX7219_Address address, uint8_t data) {

	MAX7219_NSS_ON(def);
	__MAX7219_Transmit(def, chip, address, data);
	MAX7219_NSS_OFF(def);
}

uint32_t __MAX7219_pow10(uint8_t n) {
	if (n == 0) {
		return 1;
	} else {
		return 10 * __MAX7219_pow10(n - 1);
	}
}

void _MAX7219_SetIntValue(MAX7219_Def* def, MAX7219_Value* value) {
	bool showZero = value->leadZero;
	bool useTwo = value->useTwo;
	for (int i = 7; i >= 0; i--) {
		uint8_t address = i + 1;
		uint8_t data = (value->value / __MAX7219_pow10(i)) % 10;
		if (i == 3 && useTwo) {
			showZero = value->leadZero;
		}
		if (data == 0x00) {
			if (!showZero)
				data = CodeB_EMPTY;
		} else {
			showZero = true;
		}
		__MAX7219_TransmitToChip(def, value->chip, address,
				address == value->dotDigit ? data + CodeB_DOT : data);
	}
}

void __MAX7219_Transmit(MAX7219_Def* def, uint8_t chip, uint8_t address,
		uint8_t value) {
	uint8_t lenght = chip * 2 + 2;
	uint8_t i = 0;
	while (i < chip * 2) {
		def->SPI_VALUE[i] = Address_NOOP;
		i++;
	}
	def->SPI_VALUE[i] = address;
	def->SPI_VALUE[i + 1] = value;

	MAX7219_Transmit_SPI(def, def->SPI_VALUE, lenght);
}

