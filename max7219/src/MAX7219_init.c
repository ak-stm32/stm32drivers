/*
 * MAX7219.c
 *
 *  Created on: 10Apr.,2017
 *      Author: akoiro
 */
#include "MAX7219.h"

#include "stm32f1xx.h"
#include "stm32f1xx_it.h"
#include "stm32f1xx_hal.h"
#include "stm32f1xx_hal_def.h"
#include "DWT.h"

//MX
void __MAX7219_InitDMA(MAX7219_Def*);
void __MAX7219_InitGPIO(MAX7219_Def*);
void __MAX7219_Init(MAX7219_Def* def);

//MSP
void __MAX7219_MspInitGPIO(MAX7219_Def* def);
void __MAX7219_MspInitDMA(MAX7219_Def* def);
void __MAX7219_MspDeInitDMA(MAX7219_Def* def);
void __MAX7219_MspDeInitGPIO(MAX7219_Def* def);

//Initialization
void MAX7219_MspInit(MAX7219_Def* def) {
	__AK_SPI_CLK_ENABLE(def->SPI);
	__MAX7219_MspInitGPIO(def);
	__MAX7219_MspInitDMA(def);
}

void MAX7219_MspDeInit(MAX7219_Def* def) {
	__AK_SPI_CLK_DISABLE(def->SPI);
	__MAX7219_MspDeInitGPIO(def);
	__MAX7219_MspDeInitDMA(def);
}

void MAX7219_Init(MAX7219_Def* def) {
	__MAX7219_InitGPIO(def);
	__MAX7219_InitDMA(def);
	__MAX7219_Init(def);
}

/**
 * SPI1.MOSI = GPIO_PIN_7 -> MAX7219.1.DIN
 * SPI1.SCK = GPIO_PIN_5 -> MAX7219.13.CLK
 * SPI1.NSS = GPIO_PIN_4 -> MAX7219.12.LOAD(CS)
 * SPI2.PORT = GPIOA
 * DMA1_Channel3
 * }
 */
void MAX7219_SPI1_Init(MAX7219_Def* def) {
	def->SPI = SPI1;
	def->DMA_HANDLE.Instance = DMA1_Channel3;
	def->DMA_IRQ = DMA1_Channel3_IRQn;

	def->PIN_MOSI = GPIO_PIN_7;
	def->PIN_SCK = GPIO_PIN_5;
	def->PIN_NSS = GPIO_PIN_4;

	def->PINS.Pin = def->PIN_MOSI | def->PIN_SCK;
	def->PINS.Mode = GPIO_MODE_AF_PP;
	def->PINS.Pull = GPIO_NOPULL;
	def->PINS.Speed = GPIO_SPEED_FREQ_HIGH;

	def->PORT = GPIOA;
	MAX7219_Init(def);
}

/**
 *
 * SPI2.MOSI = GPIO_PIN_15 -> MAX7219.1.DIN
 * SPI2.SCK = GPIO_PIN_13 -> MAX7219.13.CLK
 * SPI2.NSS = GPIO_PIN_12 -> MAX7219.12.LOAD(CS)
 * SPI2.PORT = GPIOB
 * DMA1_Channel5
 * }
 */
void MAX7219_SPI2_Init(MAX7219_Def* def) {
	def->SPI = SPI2;
	def->DMA_HANDLE.Instance = DMA1_Channel5;
	def->DMA_IRQ = DMA1_Channel5_IRQn;

	def->PIN_MOSI = GPIO_PIN_15;
	def->PIN_SCK = GPIO_PIN_13;
	def->PIN_NSS = GPIO_PIN_12;

	def->PINS.Pin = def->PIN_MOSI | def->PIN_SCK;
	def->PINS.Mode = GPIO_MODE_AF_PP;
	def->PINS.Pull = GPIO_NOPULL;
	def->PINS.Speed = GPIO_SPEED_FREQ_HIGH;
	def->PORT = GPIOB;
	MAX7219_Init(def);
}

void MAX7219_TxCallback(MAX7219_Def* def) {

}

void __MAX7219_InitDMA(MAX7219_Def* def) {
	__HAL_RCC_DMA1_CLK_ENABLE()
	;

	HAL_NVIC_SetPriority(def->DMA_IRQ, 0, 0);
	HAL_NVIC_EnableIRQ(def->DMA_IRQ);
}

void __MAX7219_InitGPIO(MAX7219_Def* def) {
	__AK_GPIO_PORT_ENABLE(def->PORT);
	GPIO_InitTypeDef gpio;
	gpio.Pin = def->PIN_MOSI | def->PIN_SCK;
	gpio.Mode = GPIO_MODE_AF_PP;
	gpio.Pull = GPIO_NOPULL;
	gpio.Speed = GPIO_SPEED_FREQ_HIGH;

	HAL_GPIO_Init(def->PORT, &gpio);
	gpio.Pin = def->PIN_NSS;
	gpio.Mode = GPIO_MODE_OUTPUT_PP;
	gpio.Pull = GPIO_PULLDOWN;
	gpio.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(def->PORT, &gpio);

	HAL_GPIO_WritePin(def->PORT, def->PIN_NSS, GPIO_PIN_SET);
}

void __MAX7219_Init(MAX7219_Def* def) {
	def->SPI_HANDLE.Instance = def->SPI;
	def->SPI_HANDLE.Init.Mode = SPI_MODE_MASTER;
	def->SPI_HANDLE.Init.Direction = SPI_DIRECTION_1LINE;
	def->SPI_HANDLE.Init.DataSize = SPI_DATASIZE_8BIT;
	def->SPI_HANDLE.Init.CLKPolarity = SPI_POLARITY_LOW;
	def->SPI_HANDLE.Init.CLKPhase = SPI_PHASE_1EDGE;
	def->SPI_HANDLE.Init.NSS = SPI_NSS_SOFT;
	def->SPI_HANDLE.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
	def->SPI_HANDLE.Init.FirstBit = SPI_FIRSTBIT_MSB;
	def->SPI_HANDLE.Init.TIMode = SPI_TIMODE_DISABLE;
	def->SPI_HANDLE.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	def->SPI_HANDLE.Init.CRCPolynomial = 10;
	if (HAL_SPI_Init(&def->SPI_HANDLE) != HAL_OK) {
		Error_Handler();
	}
}

void __MAX7219_MspInitGPIO(MAX7219_Def* def) {
	HAL_GPIO_Init(def->PORT, &def->PINS);
}

void __MAX7219_MspInitDMA(MAX7219_Def* def) {
	def->DMA_HANDLE.Init.Direction = DMA_MEMORY_TO_PERIPH;
	def->DMA_HANDLE.Init.PeriphInc = DMA_PINC_DISABLE;
	def->DMA_HANDLE.Init.MemInc = DMA_MINC_ENABLE;
	def->DMA_HANDLE.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	def->DMA_HANDLE.Init.MemDataAlignment = DMA_PDATAALIGN_BYTE;
	def->DMA_HANDLE.Init.Mode = DMA_NORMAL;
	def->DMA_HANDLE.Init.Priority = DMA_PRIORITY_LOW;
	if (HAL_DMA_Init(&def->DMA_HANDLE) != HAL_OK) {
		Error_Handler();
	}
	__HAL_LINKDMA(&def->SPI_HANDLE, hdmatx, def->DMA_HANDLE);
}

void __MAX7219_MspDeInitGPIO(MAX7219_Def* def) {
	HAL_GPIO_DeInit(def->PORT, def->PINS.Pin);
}

void __MAX7219_MspDeInitDMA(MAX7219_Def* def) {
	HAL_DMA_DeInit(def->SPI_HANDLE.hdmatx);
}
