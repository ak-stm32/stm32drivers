/*
 * MAX7219.c
 *
 *  Created on: 10Apr.,2017
 *      Author: akoiro
 */
#include "MAX7219_spi.h"

#include "DWT.h"

void __MAX7219_WAIT_SPI(MAX7219_Def* def);
void __MAX7219_Transmit_SPI(MAX7219_Def*, uint8_t*, uint16_t);

//logic
void MAX7219_Transmit_SPI(MAX7219_Def* def, uint8_t* data, uint16_t size) {
	MAX7219_NSS_ON(def);
	__MAX7219_Transmit_SPI(def, data, size);
	MAX7219_NSS_OFF(def);
}

void MAX7219_NSS_ON(MAX7219_Def* def) {
	__MAX7219_WAIT_SPI(def);
	HAL_GPIO_WritePin(def->PORT, def->PIN_NSS, GPIO_PIN_RESET);
}

void MAX7219_NSS_OFF(MAX7219_Def* def) {
	__MAX7219_WAIT_SPI(def);
	HAL_GPIO_WritePin(def->PORT, def->PIN_NSS, GPIO_PIN_SET);
}


void __MAX7219_WAIT_SPI(MAX7219_Def* def) {
	while (def->SPI_HANDLE.State != HAL_SPI_STATE_RESET
			&& def->SPI_HANDLE.State != HAL_SPI_STATE_READY) {
		DWT_Delay(1);
	}
}

void __MAX7219_Transmit_SPI(MAX7219_Def* def, uint8_t* data, uint16_t size) {
	HAL_StatusTypeDef status = HAL_SPI_Transmit_DMA(&def->SPI_HANDLE,
			data, size);
	if (status != HAL_OK)
		Error_Handler();
}
