/*
 * EC11.c
 *
 *  Created on: 1 Aug 2016
 *      Author: akoiro
 */

#include "EC11.h"

void EC11_Init(EC11_Def* def) {
	__AK_GPIO_PORT_ENABLE(def->PORT);

	TIM_Encoder_InitTypeDef sConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	def->htim.Instance = def->tim;
	def->htim.Init.Prescaler = 0xFFFF;
	def->htim.Init.CounterMode = TIM_COUNTERMODE_UP;
	def->htim.Init.Period = 0;
	def->htim.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;

	sConfig.EncoderMode = TIM_ENCODERMODE_TI2;

	sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
	sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
	sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
	sConfig.IC1Filter = 0xF;

	sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
	sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
	sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
	sConfig.IC2Filter = 0xF;
	if (HAL_TIM_Encoder_Init(&def->htim, &sConfig) != HAL_OK) {
		Error_Handler();
	}

	if (HAL_TIM_Encoder_Start_IT(&def->htim, TIM_CHANNEL_1 | TIM_CHANNEL_2) != HAL_OK) {
		Error_Handler();
	}

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&def->htim, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
}

void EC11_MspInit(EC11_Def* def) {
	GPIO_InitTypeDef GPIO_InitStruct;
	__AK_TIM_CLK_ENABLE(def->tim);

	GPIO_InitStruct.Pin = def->DT_PIN | def->CLK_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(def->PORT, &GPIO_InitStruct);

	HAL_NVIC_SetPriority(def->irq, 0, 0);
	HAL_NVIC_EnableIRQ(def->irq);
}

void EC11_MspDeInit(EC11_Def* def) {
    __AK_TIM_CLK_DISABLE(def->tim);
    HAL_GPIO_DeInit(def->PORT, def->DT_PIN | def->CLK_PIN);
    HAL_NVIC_DisableIRQ(def->irq);
}

void EC11_Value(EC11_Def* def) {

}

#ifndef EC11A
EC11_Direction _EC11_direction = None;
EC11_Delegator _EC11_delegator;

void _EC11_Init_Pin(GPIO_TypeDef*, uint32_t, IRQn_Type);
void _EC11_V00(EC11_Def* def);
void _EC11_V01(EC11_Def* def);
void _EC11_V10(EC11_Def* def);
void _EC11_V11(EC11_Def* def);

void EC11_Init(EC11_Def* def) {
__AK_GPIO_PORT_ENABLE(def->PORT);

_EC11_Init_Pin(def->PORT, def->CLK_PIN, def->CLK_IRQ);
_EC11_Init_Pin(def->PORT, def->DT_PIN, def->DT_IRQ);
_EC11_Init_Pin(def->PORT, def->SW_PIN, def->SW_IRQ);

def->direction = None;
}

void EC11_Read(EC11_Def* def) {
GPIO_PinState clk = HAL_GPIO_ReadPin(def->PORT, def->CLK_PIN);
GPIO_PinState dt = HAL_GPIO_ReadPin(def->PORT, def->DT_PIN);

//00,01,11,10
//00,10,11,01

EC11_Value v = 0b01 * clk + 0b10 * dt;

switch (v) {
	case V00:
	_EC11_V00(def);
	break;
	case V01:
	_EC11_V01(def);
	break;
	case V10:
	_EC11_V10(def);
	break;
	case V11:
	_EC11_V11(def);
	break;
}
}

void _EC11_Init_Pin(GPIO_TypeDef *port, uint32_t pin, IRQn_Type irq) {
GPIO_InitTypeDef _def;

_def.Pin = pin;
_def.Mode = GPIO_MODE_IT_RISING_FALLING;
_def.Speed = GPIO_SPEED_HIGH;
_def.Pull = GPIO_NOPULL;
HAL_GPIO_Init(port, &_def);

if (irq > -1) {
	HAL_NVIC_SetPriority(irq, 1, 0);
	HAL_NVIC_EnableIRQ(irq);
}
}

void _EC11_V00(EC11_Def* def) {
switch (def->direction) {
	case None:
	break;
	case Left:
	case Rigth:
	if (def->delegator) {
		def->delegator(def->direction);
	}
	def->direction = None;
	break;
}
}

void _EC11_V01(EC11_Def* def) {
switch (def->direction) {
	case None:
	def->direction = Rigth;
	break;
	case Left:
	def->direction = None;
	break;
}
}

void _EC11_V10(EC11_Def* def) {
switch (def->direction) {
	case Rigth:
	def->direction = None;
	break;
	case None:
	def->direction = Left;
	break;
}
}

void _EC11_V11(EC11_Def* def) {
}
#endif
