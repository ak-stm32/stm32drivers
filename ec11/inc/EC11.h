/*
 * EC11.h
 *
 *  Created on: 1 Aug 2016
 *      Author: akoiro
 */

#ifndef EC11_H_
#define EC11_H_

#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"
#include "COMMON.h"

#define EC11A

typedef struct {
	GPIO_TypeDef *PORT;
	uint16_t CLK_PIN;
	uint16_t DT_PIN;
	uint16_t SW_PIN;
	TIM_TypeDef* tim;
	IRQn_Type irq;

	TIM_HandleTypeDef htim;
} EC11_Def;

void EC11_Init(EC11_Def* def);
void EC11_MspInit(EC11_Def*);
void EC11_MspDeInit(EC11_Def*);

void EC11_Value(EC11_Def* def);


#ifndef EC11A
typedef enum {
	 Left = 1,
	 Rigth = 2,
	 None = 0
} EC11_Direction;


typedef enum {
	 V00 = 0,
	 V01 = 1,
	 V10 = 2,
	 V11 = 3
} EC11_Value;

typedef void (*EC11_Delegator)(EC11_Direction);

typedef struct {
	uint16_t CLK_PIN;
	IRQn_Type CLK_IRQ;
	uint16_t DT_PIN;
	IRQn_Type DT_IRQ;
	uint16_t SW_PIN;
	IRQn_Type SW_IRQ;
	GPIO_TypeDef *PORT;
	EC11_Direction direction;
	EC11_Delegator delegator;
} EC11_Def;


void EC11_Init(EC11_Def* def);
void EC11_Read(EC11_Def* def);
#endif
#endif /* EC11_H_ */

