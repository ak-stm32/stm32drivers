#include "TIM.h"

void TIM_Init(TIM_Def *def) {
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	__AK_TIM_CLK_ENABLE(def->Instance);

	def->Handle.Instance = def->Instance;
	def->Handle.Init.Prescaler = def->Prescaler;
	def->Handle.Init.CounterMode = TIM_COUNTERMODE_UP;
	def->Handle.Init.Period = def->Period;
	def->Handle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	def->Handle.Init.RepetitionCounter = 0;
	HAL_TIM_Base_Init(&def->Handle);

	HAL_TIM_Base_Start_IT(&def->Handle);

	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource(&def->Handle, &sClockSourceConfig);

	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization(&def->Handle, &sMasterConfig);
}

void TIM_MspInit(TIM_Def *def) {
	HAL_NVIC_SetPriority(def->IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(def->IRQn);
}

void TIM_IRQHandler(TIM_Def *def) {
	HAL_TIM_IRQHandler(&def->Handle);
}

void TIM2_Init(TIM_Def* def) {

	def->Instance = TIM2;
	def->Prescaler = 45000;
	def->Period = 1;
	def->Instance = TIM2;
	def->IRQn = TIM2_IRQn;

	TIM_Init(def);
}
