/*
 * TIM.h
 *
 *  Created on: 30 Jul 2016
 *      Author: akoiro
 */

#ifndef TIM_H_
#define TIM_H_
#include "stm32f1xx_hal.h"
#include "COMMON.h"

typedef struct TIM_Def{
	TIM_TypeDef *Instance;
	IRQn_Type IRQn;

	uint32_t Prescaler;
	uint32_t Period;

	TIM_HandleTypeDef Handle;
} TIM_Def;

void TIM_Init(TIM_Def*);
void TIM_IRQHandler(TIM_Def*);
void TIM_MspInit(TIM_Def*);

void TIM2_Init(TIM_Def*);

#endif /* TIM_H_ */
